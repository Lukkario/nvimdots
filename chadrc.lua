---@type ChadrcConfig
local M = {}

M.ui = { theme = 'doomchad' }
M.mappings = require "custom.mappings"
M.plugins = "custom.plugins"

return M
