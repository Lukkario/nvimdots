local M = {}

-- In order to disable a default keymap, use
M.disabled = {
  n = {
      ["<leader>q"] = ""
  }
}

M.nvimtree = {
  plugin = true,

  n = {
  },
}
-- Your custom mappings
M.abc = {
  n = {
   ["<S-Left>"] = { "<C-w>h", "Window left" },
   ["<S-Right>"] = { "<C-w>l", "Window right" },
   ["<S-Down>"] = { "<C-w>j", "Window down" },
   ["<S-Up>"] = { "<C-w>k", "Window up" },
   ["<leader>e"] = { "<cmd> NvimTreeToggle <CR>", "Toggle nvimtree" },
   ["<C-e>"] = { "<cmd> NvimTreeFocus <CR>", "Focus nvimtree" },
   ["<leader>|"] = { "<cmd> vspl <CR>", "Vertical split" },
   ["<leader>_"] = { "<cmd> spl <CR>", "Horizontal split" },
   ["<leader>q"] = {
     function()
       require("nvchad.tabufline").close_buffer()
     end,
     "Close buffer",
   },
  -- ["<leader>oq"] = {
  --   function ()
  --     require("nvchad.tabufline").order
  --   end,
  --   "Close other buffers"
  --   },
  },

  i = {
  }
}

return M
